# m1_ter [[repo](https://gitlab.com/mymessaoudi/m1_ter)]

+ **Sujet**: *Théorème d’interpolation de Carleson et applications*[[pdf](memoire/documents/sujet.pdf)]
+ **Professeur référent**: William Alexandre [[mail](mailto:william.alexandre@univ-lille.fr)]
+ **Mémoire**: [[pdf](memoire/master.pdf)][[tex](https://gitlab.com/mymessaoudi/m1_ter/-/archive/main/m1_ter-main.zip?path=memoire)] (à compiler avec pdfLatex)

## Résumé sujet

Nous proposons dans ce mémoire de démontrer le théorème d'interpolation Carleson à savoir:

> Soit <img src ="https://render.githubusercontent.com/render/math?math=(z_{j})_{j}"> une suite infinie de points du cercle unité <img src ="https://render.githubusercontent.com/render/math?math=\Delta"> de <img src ="https://render.githubusercontent.com/render/math?math=\mathbb{C}">. Alors les conditions suivantes sont équivalentes:
>
> + Quelle que soit la suite <img src ="https://render.githubusercontent.com/render/math?math=(w_j)_j\subset \mathbb{C}"> bornée, il existe <img src ="https://render.githubusercontent.com/render/math?math=f"> holomorphe et bornée sur <img src ="https://render.githubusercontent.com/render/math?math=\Delta"> telle que pour tout <img src ="https://render.githubusercontent.com/render/math?math=j,\ f(z_j)=w_j">.
>
> + <img src ="https://render.githubusercontent.com/render/math?math=\delta=\inf_{n\geq 1}\prod_{j\neq n}^{} \left| \frac{z_j-z_n}{1-\bar{z_j}z_n} \right| >0">.

Nous étudierons aussi quelques applications de ce théorème.  
Cette étude sera  basée sur le livre de "Analyse Mathématique. Grands théorèmes du 20ème siècle" de D. Choimet et H.Queffélec.

<!-- ## A faire -->

<!-- + Montrer que <img src ="https://render.githubusercontent.com/render/math?math=H^p"> est un evn complet -->
<!-- + Finir factorisation de Blaschke -->
<!-- + --> 

## Questions 

