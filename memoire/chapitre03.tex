%title: Le théorème d'interpolation de Carleson
%! TEX root = master.tex

%========================
\section{Interpolations de suite bornées}
\label{sec:théorème_d'interpolation_de_carleson}
%========================

%========================
\subsection{Théorème de Carleson}
\label{ssec:théorème_de_carleson}
%========================

%------------------------
\begin{definition}
	Soit $(z_{n})_{n}$ une suite de points complexes, on pose pour $n\in \N$ 
	\begin{align*}
		\delta_n:=\prod_{i\neq n}^{} \d(z_{i},z_{n}) ,
	\end{align*}
	on dit alors que $(z_{n})$ est uniformément séparée si $\inf_{n\geq 0}\delta_n>0$.
\end{definition}
%------------------------
\begin{remark}\label{rem:blaschke-coeff}
	On a pour tout $n\in \N,\ \delta_{n}=(1- |z_{n}|^2)|B'(z_{n})|$ où $B$ est le produit de Blaschke associée à $(z_{n})$.
	En effet, puisque $B(z_n)=0$, on peut écrire
	\begin{align*}
		\frac{B(z)-B(z_{n})}{z_{n}-z}=\frac{|z_{n}|}{z_{n}(1-\bar{z_{n}}z)}\prod_{i\neq n}^{} \frac{|z_{i}|}{z_{i}} \frac{z_{i}-z}{1-\bar{z_{i}}z}=\frac{|z_{n}|}{z_{n}(1-\bar{z_{n}}z)}\delta_n ,
	\end{align*}
	en passant la valeur absolue et en faisant tendre $z\to z_{n}$ on obtient
	\begin{align*}
		|B'(z_{n})|=\frac{\delta_{n}}{1-|z_{n}|^2},
	\end{align*}
	d'où le résultat.
\end{remark}



%------------------------
\begin{theorem}[de Carleson]\label{thrm:de_carleson}
	Soit $(z_{i})_{i\geq 0}$ une suite de points distincts de $\D$. Les assertions suivantes sont équivalentes:
	\begin{enumerate}[label=(\roman*),leftmargin=*]
		\item Pour toute suite bornée $w=(w_{i})_{i\geq 0}$ de points de $\C$, il existe une fonction $f\in \mathbb{H}^{\infty}$ telle que pour tout $i\geq 0,\ f(z_{i})=w_{i}$.
		\item La suite $(z_{i})_{i\geq 0}$ est uniformément séparée, c'est-à-dire
			\begin{align*}
				\delta:=\inf_{n\geq 0}\delta_n=\inf_{n\geq 0}\prod_{i\neq n}^{} \d(z_{i},z_{n})>0  .
			\end{align*}
	\end{enumerate}
	Si l'une des conditions est vérifiées, on dit que $(z_{i})_{i\geq 0}$ est une \textit{suite d'interpolation}.
\end{theorem}

%------------------------
\begin{proof}
	Supposons (i) vérifié, alors l'application $T: H^{\infty} \to \ell^{\infty},\ f \longmapsto (f(z_{i}))_{i\geq 0}$ est surjective, elle est bien évidemment linéaire et pour toute fonction $f\in H^{\infty}$ on a 
	\begin{align*}
		 \norm{T(f)}_\infty=\sup_{n\geq 0}|f(z_{n})|\leq\sup_{x\in \D} |f(x)|=\norm{f}_\infty,
	\end{align*}
	donc $T$ est continue .
	On a une fonction linéaire surjective continue d'un espace complet dans un espace complet, on peut appliquer le théorème de l'application ouverte qui nous donne l'existence de $C>0$ telle que pour toute suite $w\in \ell^{\infty}$ il existe $f\in H^{\infty}$ telle que 
	\begin{align*}
		T(f)=w\text{ et }\norm{f}_\infty\leq C\norm{w}_\infty .
	\end{align*}
	On définit alors pour $n\in \N$ la suite $w^{(n)}\in \ell^{\infty}$ telle que $w^{(n)}_{i}=0$ si $i\neq n$ et 1 sinon. On a alors l'existence de $f_{n}\in H^\infty$ telle que 
	\begin{align*}
		f_{n}(z_{n})=1,\ f_{n}(z_{i})=0 \text{ si }i\neq n\text{ et }\norm{f_{n}}_\infty\leq C .
	\end{align*}
	%TODO: compléter la citation du théorème 
	Alors $f$ est non nulle et la suite $(z_{i})_{i\in \N\setminus \{n\} }$ est une suite de zéros de $f_{n}$, on peut donc appliquer le théorème \ref{thrm:factorisation_de_blaschke} , qui nous donne l'existence de $g_{n}\in H^{\infty}$ telle que 
	\begin{align*}
		f_{n}=B_{n}g_{n}\text{ et }\norm{g_{n}}_\infty=\norm{f_{n}}_\infty\leq C ,
	\end{align*}
	où $B_{n}$ est le produit de Blaschke associé à la suite $(z_{i})_{i\in \N\setminus \{n\} }$.

	On a alors
	\begin{align*}
		1=|f_{n}(z_{n})|=|B_{n}(z_{n})|\times |g_{n}(z_{n})|\leq C|B_{n}(z_{n})|,
	\end{align*}
	donc $|B_{n}(z_{n})|\geq C^{-1}$, or $B_{n}$ est la produit de Blaschke associé à la suite $(z_{i})_{i\in \N\setminus \{n\} }$ donc $|B_{n}(z_{n})|=\prod_{i\neq n}^{} \d(z_{n},z_{i})=\delta_{n} $, donc $\delta=\inf_{n\geq 0}\delta_{n}\geq C^{-1}>0$.
\end{proof}

La seconde implication étant plus compliqué, introduisant dans un premier temps un lemme utile. On supposera dans la suite que (ii) est vérifié, i.e. $\delta>0$.

%------------------------
\begin{lemma}[Carleson]\label{lemma:carleson}
	Soit $(z_{n})_{n}$ une suite uniformément séparante avec $\delta$ pour constante de séparation uniforme, on pose pour $j\geq 1,\ d_j=1-|z_{j}|^2$.
	Alors la mesure $\mu=\sum_{i=0}^{\infty} (1-|z_{i}|^2)\delta_{z_{i}}$ est une mesure de Carleson, i.e. qu'il existe $C_{\delta}>0$ tel que
	\begin{align}\label{ineq:carleson1}
		\forall F\in H^2,\ \int_{\D}^{} |F|^2\d\mu=\sum_{i=1}^{\infty} d_i|F(z_{i})|^2\leq C_\delta \norm{F}_2^2 
	\end{align}
	\begin{align}\label{ineq:carleson2}
		\forall F\in H^1,\ \int_{\D}^{} |F|\d\mu=\sum_{i=1}^{\infty} d_i|F(z_{i})|\leq C_\delta \norm{F}_1
	\end{align}
\end{lemma}
%------------------------
\begin{proof}
	$(z_{n})_n$ étant $\delta$ séparante, on a 
	\begin{align*}
		|z_{i}-z_{k}|\geq \delta|1-\bar{z_{i}}z_{k}|\geq \delta(1-|z_{i}| |z_{k}|)\geq \frac{\delta}{2}(d_{i}+d_{k}) .
	\end{align*}
	car $d_{i}+d_{k}=2-|z_{i}|^2+|z_{k}|^2$ donc, puisque $2|z_{i}| |z_{k}|\leq |z_{i}|^2+|z_{k}^2|$, on a l'inégalité.
	Il vient alors que les disques $\Delta_i:=D\left( z_{i},\frac{\delta}{4}d_{i} \right)$ sont deux à deux disjoints, de plus, puisque $d_{i}<2(1-|z_{i}|)$ on a 
	\begin{align*}
		|z_{i}|+\frac{\delta}{4}d_{i}\leq |z_{i}|+\frac{d_{i}}{2}<|z_{i}|+1-|z_{i}|=1,
	\end{align*}
	donc les disques sont mêmes contenues dans $\D$.
	Soit $F\in H^2$. En posant $G=F'^2\in H^{1}$ on a pour $a\in \D $ et $\rho\in [0,r[$ tel que $D(a,r)\subset \D $ par la formule de Cauchy
	\begin{align*}
		|G(a)|=\left| \frac{1}{2\pi}\int_{0}^{2\pi} G(a+\rho\e^{it})\dt   \right|\leq \frac{1}{2\pi}\int_{0}^{2\pi} |G(a+\rho\e^{it})|\dt   .
	\end{align*}
	En multipliant par $\rho$ et en intégrant de 0  à $r$ par rapport à $\rho$ 
	\begin{align*}
		\int_{0}^{r} \rho|G(a)|\d\rho=\frac{r^2}{2}|G(a)|\leq \frac{1}{2\pi}\int_{0}^{r} \int_{0}^{2\pi} |G(a+r\e^{it})\rho \dt\d\rho=\frac{1}{2\pi}\int_{D(a,r)}^{} G(z)\d\lambda(z).
	\end{align*}
	Donc en notant $\d A(z)=\frac{\d\lambda(z)}{\pi}$ la mesure de Lebesgue normalisé sur $\D $, on a pour tout disque $D(a,r)\subset \D $,
	\begin{align*}
		|G(a)|\leq \frac{1}{r^2}\int_{D(a,r)}^{} |G(z)|\d A(z).
	\end{align*}
 	Puisque les disques $\Delta_i=D\left( z_{i},\frac{\delta}{4}d_{i} \right)\subset \D $ on a en particulier
	\begin{align*}
		d_{i}^3|F'(z_{i})|^2\leq 16\delta^{-2}d_{i}\int_{\Delta_{i}}^{} |F'(z)|^2\d A(z)  .
	\end{align*}
	Et puisque 
	\begin{align*}
		d_{i}&= 1-|z|^2+|z|^2-|z_{i}|^2 \nonumber\\
		&\leq  1-|z|^2+(|z|+|z_{i}|) | |z|-|z_{i}| | \nonumber\\
		&\leq 1-|z|^2+2|z-z_{i}| \nonumber\\
		&\leq  1-|z|^2+\frac{1}{2}d_{i} \nonumber,
	\end{align*}
	donc $d_{i}\leq 2(1-|z|^2)$, on a l'inégalité 
	\begin{align*}
		d_{i}^3|F'(z_{i})|^2\leq 32\delta^{-2}\int_{\Delta_{i}}^{} (1-|z|^2)|F'(z)|^2\d A(z)  .
	\end{align*}
	Les disques $(\Delta_{i})_i$ étant deux à deux disjoints et tous inclut dans $\D $ on a, en sommant l'inégalité précédente
	\begin{align*}
		\sum_{i=0}^{\infty} d_{i}^3|F'(z_{i})|^2\leq 32\delta^{-2}\sum_{i=0}^{\infty} \int_{\Delta_{i}}^{} (1-|z|^2)|F'(z)|^2\d A(z)
		\leq 32\delta^{-2}\int_{\D }^{} (1-|z|^2)|F'(z)|^2\d A(z)   .
	\end{align*}
	En notant maintenant $F(z)=\sum_{n=0}^{\infty} c_{n}z^{n}$ on a en utilisant les coordonnées polaires 
	\begin{align*}
		\int_{\D }^{} (1-|z|^2)|F'(z)|^2\d A(z)&=\int_{0}^{1} \frac{1}{2\pi}\int_{0}^{2\pi} 2(1-r^2)|F'(r\e^{it})|\dt \d r \nonumber\\
		&=\int_{0}^{1} 2(1-r^2) \frac{1}{2\pi}\int_0^{2\pi}|F'(r\e^{it}|^2 \dt    \nonumber\\
	\end{align*}
	Et en utilisant la formule de Parseval, puisque $F'(z)=\sum_{n=1}^{\infty} nc_{n}z^{n-1}$ on va avoir
	\begin{align*}
		\int_{\D }^{} (1-|z|^2)|F'(z)|^2\d A(z)&=\int_0^{1}2(1-r^2)\sum_{n=1}^{\infty} n^2|c_{n}|^2r^{2n-1}\d r\\
		&= \sum_{n=1}^{\infty} n^2|c_{n}|^2\cdot  2\int_0^{1}r^{2n-1}-r^{2n+1}\d r \nonumber\\
		&= \sum_{n=1}^{\infty} n^2|c_{n}|^2\cdot  2\left[ \frac{r^{2n}}{2n}-\frac{r^{2n+2}}{2(n+1)} \right]_0^{1} \nonumber\\
		&= \sum_{n=1}^{\infty} \frac{n^2|c_{n}^2|}{n(n+1)}\leq \sum_{n=1}^{\infty} |c_{n}|^2\leq \norm{F}_2^{2} \nonumber
	\end{align*}
	Donc finalement on aura 
	\begin{align*}
		\sum_{i=1}^{\infty} d_{i}^3|F'(z_{i})|^2\leq 32\delta^{-2}\norm{F}_2^2 .
	\end{align*}
	Pour la seconde inégalité \eqref{ineq:carleson2}, considérons $G=BF$ où $B$ est le produit de Blaschke associé à la suite $(z_{i})_i$ et $F\in H^{2}$.
	On a alors $G\in H^2$ et $G'(z_{i})=B'(z_{i})F(z_{i})+B(z_{i})F'(z_{i})=B'(z_{i})F(z_{i})$ puisque les $z_{i}$ sont des zéros de $B$. 
	En appliquant l'inégalité \eqref{ineq:carleson1} à $G$ on obtient
	\begin{align*}
		\sum_{i=1}^{\infty} d_{i}^3|B'(z_{i})|^2|F(z_{i})|^2\leq 32\delta^{-2}\norm{G}_2^2=32\delta^{-2}\norm{F}_2^2 .
	\end{align*}
	Cependant on a vu remarque \ref{rem:blaschke-coeff}, $|B'(z_{i})|=\frac{\delta_{i}}{d_{i}}\geq \frac{\delta}{d_{i}}$ on obtient alors de l'inégalité précédente
	\begin{align*}
		\sum_{i=1}^{\infty} d_{i}|F(z_{i})|^2\leq 32\delta^{-4}\norm{F}_2^2 .
	\end{align*}
	Ainsi pour $F\in H^{1}$ par le théorème \ref{thrm:factorisation_de_blaschke} de factorisation de Blaschke , on peut écrire $F=GH$ pour $G,H\in H^{2}$ tels que $\norm{F}_1=\norm{G}_2\norm{H}_2$ et donc, avec l'inégalité de Cauchy Schwarz et l'inégalité précédente, on a   
	\begin{align*}
		\sum_{i=1}^{\infty} d_{i}|F(z_{i})|&\leq \left( \sum_{i=1}^{\infty} d_{i}|G(z_{i})|^2 \right)^{1 /2}\left( \sum_{i=1}^{\infty} d_{i}|H(z_{i})|^2 \right)^{1 /2}\\
		&\leq 32\delta^{-4}\norm{G}_2\norm{H}_2=32\delta^{-4}\norm{F}_1 \nonumber\\
	\end{align*}
	d'où l'inégalité \eqref{ineq:carleson2}. 
	\end{proof}
On peut maintenant donner une preuve de la condition suffisante du théorème \ref{thrm:de_carleson} de Carleson.

Soit $n\in \N$ fixé, on pose le problème d'interpolation fini ci dessus
\begin{align}\label{eq:pb-fini}
	\forall 0\leq i\leq n,\ f_{n}(z_{i})=w_{i} .
\end{align}

On pose $g=g_{n}\in H^{\infty}$ une solution de \eqref{eq:pb-fini}, on peut prendre l'interpolation de Lagrange par exemple.
On pose $E_{n}$ l'ensemble des fonctions bornées sur le disque $\D $ solution de $\eqref{eq:pb-fini}$.
En notant $B_{n}$ le produit de Blaschke finie associé à la suite de points $(z_{i})_{0\leq i\leq n}$, on a alors $E_{n}=g+B_{n}H^{\infty}$ puisque 
pour $f\in E_{n}$, $f-g\in H^{\infty}$ et a pour zéro $(z_{i})_{0\leq i\leq n}$ donc par factorisation de Blaschke on a l'existence de $h\in H^{\infty}$ tel que $f=g+B_{n}h$, l'inclusion réciproque étant évidente on a égalité.
De plus, la norme $L^{\infty}$ pouvant s'évaluer aussi bien sur le cercle que dans le disque unité, et puisque $\norm{B_{n}}_{\infty}=1$ sur $\partial \D $ on va avoir
\begin{align}\label{eq:estimation-Mn}
	M_{n}=\inf_{f\in E_{n}}\norm{f}_\infty=\inf_{h\in H^{\infty}}\norm{g+hB_{n}}=\inf_{h\in H^{\infty}}\norm{\frac{g}{B_{n}}+h}.
\end{align}





La dernière expression étant en fait la norme quotient $\norm{\cdot }_{L^{\infty} /H^{\infty}}$ de $\frac{g}{B_{n}}$, on va donc exploiter la dualité $L^{1}-L^{\infty}$ vu en section \ref{ssec:dualité_dans_les_espaces_de_banach} pour estimer cette norme.
On aura, avec les notations de la section \ref{ssec:dualité_dans_les_espaces_de_banach}, $X=L^{1}$ qui est bien de Banach et , $E=H^{1}$ qui est bien fermé dans $X$ car sous-espace vectoriel strict.
Un résultat de la théorie de la mesure nous donne alors $X^{*}=L^{\infty}$ par la dualité
\begin{align*}
	\scal{f}{F}=\frac{1}{2\pi i}\int_{\partial \D }^{} f(z)F(z)\d z =\frac{1}{2\pi}\int_{0}^{2\pi} f(\e^{it})F(\e^{it})\e^{it}\d t ,
\end{align*}
pour $f\in L^{\infty}$ et $F\in L^{1}$.
De plus, pour $n\in \N,\ e_{n}: t \longmapsto \e^{itn}$ est dans $L^{1}$ et  $\scal{f}{e_{n}}=\frac{1}{2\pi}\int_{0}^{2\pi} f(\e^{it})\e^{it(n+1)} =\hat{f}(-n-1)$ donc, puisque d'apres la proposition \ref{prop:densite_pol+}, $\text{Pol}_+$ est dense dans $H^{1}$, on aura
\begin{align*}
	E^{\perp}&= \{f\in L^{\infty} \mid \forall n\geq 0,\ \scal{f}{e_{n}}=0 \}  \nonumber\\
	&= \{f\in L^{\infty} \mid \forall n\geq 0,\ \hat{f}(-n-1)=0\}  \nonumber\\
	&= \{f\in L^{\infty} \mid \forall n\leq -1,\ \hat{f}(n)=0\}  \nonumber\\
	&= H^{\infty} \nonumber.
\end{align*}
La dernière égalité venant de la proposition \ref{prop:hardy-iso}.
On peut alors appliquer la formule de dualité, proposition \ref{prop:formule_dualité}, à l'expression \eqref{eq:estimation-Mn} qui donne
\begin{align*}
	M_{n}=\inf_{h\in H^{\infty}}\norm{\frac{g}{B_{n}}+h}_\infty=\norm{\frac{g}{B_{n}}}_{L^{\infty} /H^{\infty}}=\sup_{\substack{F\in H^{1}\\ \norm{F}_1\leq 1}}\left| \frac{1}{2i\pi}\int_{\D }^{} \frac{g(z)F(z)}{B_{n}(z)}\d z  \right| .
\end{align*}

La fonction $H=\frac{gF}{B_{n}}$ à l'intérieur de l'intégrale étant une fonction méromorphe sur $\D\setminus \{z_0,z_1,\ldots,z_{n}\} $, son résidus en $z_{i}$ est
\begin{align*}
\text{Res}(H,z_{i})=\lim_{z \to z_{i}}(z-z_{i})H(z)=\frac{g(z)F(z)}{\frac{B_{n}(z)-B_{n}(z_{i})}{z-z_{i}}}=\frac{g(z_{i})F(z_{i})}{B_{n}'(z_{i})} 
\end{align*} 
et on a par théorème des résidus
\begin{align*}
	 M_{n}=\sup_{\substack{F\in H^{1}\\ \norm{F}_1\leq 1}}
		 \left| \sum_{i=0}^{n} \frac{g(z_{i})F(z_{i})}{B_{n}'(z_{i})} \right|=\sup_{\substack{F\in H^{1}\\ \norm{F}_1\leq 1}} \left| \sum_{i=0}^{n} \frac{w_{i}F(z_{i})}{B_{n}'(z_{i})} \right|.
\end{align*}
Un calcul identique que dans la remarque \ref{rem:blaschke-coeff}, fait avec le produit de Blaschke fini, donne pour $0\leq i\leq n$ 
\begin{align*}
	(1-|z_{i}|^2)|B_{n}'(z_{i})|=\prod_{\substack{k=0\\ k\neq i}}^{n} \d(z_{i},z_{k})\geq \delta_{i}\geq \delta   .
\end{align*}
Et donc $\frac{1}{B_{n}'(z_{i})}\leq \frac{d_{i}}{\delta}$, d'où
\begin{align*}
	M_{n}\leq \delta^{-1}\norm{w}_\infty \sup_{\substack{F\in H^{1}\\ \norm{F}_1\leq 1}}\sum_{i=0}^{n} d_{i}|F(z_{i})|\leq \delta^{-1}\norm{w}_\infty \sup_{\substack{F\in H^{1}\\ \norm{F}_1\leq 1}}\sum_{i=0}^{\infty} d_{i}|F(z_{i})|.
\end{align*}
Et puisque $F\in H^{1}$ l'inégalité \ref{ineq:carleson2} du lemme \ref{lemma:carleson} nous donne  
\begin{align*}
	M_{n}\leq \delta^{-1}\norm{w}_\infty \sup_{\substack{F\in H^{1}\\ \norm{F}_1\leq 1}}\sum_{i=0}^{\infty} d_{i}|F(z_{i})|\leq C\norm{F}_1,
\end{align*}
où $C=32\delta^{-5}\norm{w}_\infty$.

On a donc majoré les solutions de $E_{n}$ par une constante ne dépendant pas de $n$. 
Du théorème de Montel, on peut extraire de la suite $(f_{n})_{n}$ une sous-suite $(f_{n_{k}})$de fonctions convergente uniformément sur tout compact de $\D $ vers une fonction $f\in H^{\infty}$ tel que $\norm{f}_\infty\leq C$. 
Puisque la suite $(f_{n_{k}})_{n_{k}}$ vérifie le problème \eqref{eq:pb-fini}, on a 
\begin{align*}
	\forall 0\leq i\leq n_{k},\ f_{n_{k}}(z_{i})=w_{i},
\end{align*}
et donc en faisant tendre $n_{k}$ vers l'infini, 
\begin{align*}
	\forall i\in \N,\ f(z_{i})=w_{i} .
\end{align*}
\hfill\qed
%------------------------
\begin{remark}
	Si on ne demandait pas à $f$ d'être  dans $H^{\infty}$, on aurait put faire "à la Lagrangienne" en posant $L_k(z)=\frac{B(z)}{(z-z_{k})B'(z_{k})}$.
	On aurait, pour $n\neq k,\ L_k(z_{n})=\frac{B(z_{n}}{(z_{k}-z_{n})B'(z_{k})}=0$ et pour $k=n$ on aurait $L_{k}(z_{k})=\lim_{z \to z_{k}} \frac{B(z)-B(z_{k})}{z-z_{k}}\cdot \frac{1}{B'(z_{k})}=1$. 
	De plus, puisque $|B'(z_{k})|^{-1}\leq \frac{d_{k}}{\delta}=\frac{1-|z_{k}|^2}{\delta}$ et par hypothèse, $\sum_{n=1}^{\infty} (1-|z_{n}|)<\infty$, on aura bien convergence et la fonction
	 \begin{align*}
		f=\sum_{n=1}^{\infty} w_{k}L_{k},
	\end{align*}
	sera bien définit $\D $ et résout le problème d'interpolation.
\end{remark}


%========================
\subsection{Suites d'interpolations, et exemples}
\label{ssec:suites_d'interpolations}
%========================

On a défini une suite de points de $\D $ comme d'interpolation, si elle vérifie une des conditions du théorème \ref{thrm:de_carleson} de Carleson.
Bien que la condition (ii) soit difficile à vérifier, elle reste plus simple que la (i) et est faisable.
On va voir dans cette section le théorème de Hayman-Newman qui donne une condition suffisante pour avoir une suite d'interpolation.

%------------------------
\begin{theorem}[Hayman-Newman]\label{thrm:hayman-newman}
	Soit $(z_{i})_{i}$ une suite de points de $\D $ telle que
	\begin{align*}
		\forall i\geq 1,\ \frac{1-|z_{i+1}|}{1-|z_{i}|}\leq c<1 .
	\end{align*}
	Alors la suite $(z_{i})_{i}$ est une suite d'interpolation et sa constante de séparation uniforme $\delta$ vérifie que 
	\begin{align*}
		\delta\geq \prod_{n=1}^{\infty} \left( \frac{1-c^{n}}{1+c^{n}} \right)^2=:P_c  .
	\end{align*}
	En particulier, la suite $(1-c_{i})_{i\geq 1}$ est une suite d'interpolation.
\end{theorem}
%------------------------
\begin{proof}
	Soit $i>n\geq 1$, on a par hypothèse
	\begin{align*}
		1-|z_{i}|\leq c(1-|z_{i-1}|)\leq c^2(1-|z_{i-2}|)\leq \cdots\leq c^{i-n}(1-|z_{n}|) .
	\end{align*}
	Il vient, 
	\begin{align*}
		1-c^{i-n}\leq |z_{i}|-|z_{n}|+|z_{n}|(1-c^{i_n}) .
	\end{align*}
	Donc 
	\begin{align*}
		(1-|z_{n}|)(1-c^{i-n})\leq |z_{i}|-|z_{n}| .
	\end{align*}
	De même, on aura
	\begin{align*}
		1-|z_{i}| |z_{n}|&\leq 1- |z_{n}|(1-c^{i-n}(1-|z_{n}|))\\
		&= 1-|z_{n}|+c^{i-n}|z_{n}|(1-|z_{n}|) \nonumber\\
		&\leq 1-|z_{n}|+c^{i-n}(1-|z_{n}|) \nonumber\\
		&= (1-|z_{n}|)(1+c^{i-n}) \nonumber,
	\end{align*}
	donc,
	\begin{align*}
		\d(|z_{i}|,|z_{n}|)=\left| \frac{|z_{i}|-|z_{n}|}{1-|z_{i}| |z_{n}|} \right|\geq \frac{1-c^{i-n}}{1+c^{i-n}} .
	\end{align*}
	Ensuite, puisque $|1-|a| |b| |\leq |1-\bar{a}b|$ on aura
	\begin{align*}
		1-\d(a,b)^2=\frac{(1-|a|^2)(1-|b|^2)}{|1-\bar{a}b|^2}\leq \frac{(1-|a|^2)(1-|b|^2)}{(1-|a| |b|)^2}=1-\d(|a|,|b|)^2 ,
	\end{align*}
	donc $\d(a,b)\geq \d(|a|,|b|)$. Il vient finalement
	\begin{align*}
		\delta_{n}=\prod_{i\neq n}^{} \d(z_{i},z_{n})&= \prod_{i<n}^{} \d(z_{i},z_{n})\cdot \prod_{i>n}^{} \d(z_{i},z_{n})   \nonumber\\
		&\geq \prod_{i<n}^{} \d(|z_{i}|,|z_{n}|)\cdot \prod_{i>n}^{} \d(|z_{i}|,|z_{n}|)   \nonumber\\
		&\geq \prod_{i<n}^{} \left( \frac{1-c^{n-i}}{1+c^{n-i}} \right)\cdot \prod_{i>n}^{} \left( \frac{1-c^{i-n}}{1+c^{i-n}} \right)\geq P_c   \nonumber.
	\end{align*}
	et puisque $P_c>0$ car $c<1$ on a bien $(z_{i})$ suite d'interpolation.
\end{proof}
La condition devient nécessaire lorsque l'on ajoute une condition aux $(z_{i})_i$.
%------------------------
\begin{theorem}[Hayman-Newman CNS]\label{thrm:hayman-newman_cns}
	Soit $(z_{i})_{i\geq 1}$ une suite de points de $]0,1[$ croissante.
	Alors $(z_{i})_{i\geq 1}$ est une suite d'interpolation si, est seulement si, il existe $c\in ]0,1[$ tel que 
	\begin{align*}
		\forall i\geq 1,\ \frac{1-z_{i+1}}{1-z_{i}}\leq c .
	\end{align*}
\end{theorem}
%------------------------
\begin{proof}\ 
	\begin{itemize}[label=\textbullet,leftmargin=*]
		\item Si $(z_{i})$ est uniformément séparée, elle est séparée, il existe donc $0<\delta<1$ tel que 
	 \begin{align*}
		\delta\leq \frac{z_{i+1}-z_{i}}{1-z_{i}z_{i+1}} .
	\end{align*}
	Donc, $\delta\leq \frac{z_{i+1}-z_{i}}{1-z_{i}}$ car $z_{i}z_{i+1}\leq z_{i}$, donc $z_{i+1}\geq z_{i}+\delta(1-z_{i})=\delta+z_{i}(1-\delta)$  donc $1-z_{i+1}\leq 1-\delta-z_{i}(1-\delta)=(1-z_{i})(1-\delta)$, il vient 
	\begin{align*}
		 \forall i\geq 1,\ 0<\frac{1-z_{i+1}}{1-z_{i}}\leq 1-\delta<1
	\end{align*}
\item La réciproque est conséquence directe dur théorème \ref{thrm:hayman-newman} précèdent.\qedhere
	\end{itemize}	
\end{proof}
%------------------------
Ce théorème est utile pour montrer que des suites ne sont pas d'interpolation.
%------------------------
\begin{example}
	\begin{itemize}[label=\textbullet,leftmargin=*]
		\item La suite $z_n=1-2^{-n}$ est d'interpolation, en effet, on a pour tout $n\geq 1$,
			 \begin{align*}
				\frac{1-|1-2^{-n-1}|}{1-|1-2^{-n}|}=\frac{2^{n}}{2^{n+1}}=\frac{1}{2}<1 .
			\end{align*}
			donc d'après le théorème \ref{thrm:hayman-newman}, $(z_{n})_n$ est bien une suite d'interpolation.
		\item La suite $z_{n}=1-\e^{-\sqrt{n} }$ n'est pas d'interpolation. En effet, elle est bien croissante à valeur dans $]0,1[$ et on a pour $n\geq 1$ 
			\begin{align*}
				\frac{1-(1-\e^{-\sqrt{n+1} })}{1-(1-\e^{-\sqrt{n} })}=\e^{\sqrt{n}-\sqrt{n+1}}\sim_{n \infty}=1 .
			\end{align*}
			Il ne peut y avoir donc $c\in ]0,1[$ tel que $\frac{1-z_{n+1}}{1-z_{n}}\leq c$, donc le théorème \ref{thrm:hayman-newman_cns} nous assure que $(z_{n})_{n\geq 1}$ n'est pas d'interpolation.
			Et pourtant elle vérifie la condition de Blaschke, puisque $\sum_{n=1}^{\infty} \e^{-\sqrt{n} }<\infty$.
	\end{itemize}
\end{example}















